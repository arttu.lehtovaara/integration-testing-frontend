import HexToRgb from './components/HexToRgb'
import RgbToHex from './components/RgbToHex'

const App = () => {
  
  return (
    <>
      <h2>REST API</h2>
      <RgbToHex />
      <HexToRgb />
    </>
  );
}

export default App;
