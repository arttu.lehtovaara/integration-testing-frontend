import axios from 'axios'

const getHex = (newRed, newGreen, newBlue) => {
  const req = axios.get(`/rgb-to-hex?red=${newRed}&green=${newGreen}&blue=${newBlue}`, newRed, newGreen, newBlue)
  return req.then(res => res.data)
} 
const getRgb = (newRgb) => {
  const req = axios.get(`/hex-to-rgb?hex=${newRgb}`, newRgb)
  return req.then(res => res.data)
}
export default {getHex, getRgb}