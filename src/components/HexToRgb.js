import { useState } from 'react'
import service from '../service/axios'

const HexToRgb = () => {

  const [ data, setData ] = useState()
  const [ newRgb, setNewRgb ] = useState()

  const handleRgbChange = (event) => {
    setNewRgb(event.target.value)
  }

  const handleButtonClick = () => {
    service  
      .getRgb(newRgb)
        .then(data => {
          setData(data)
    })
    setNewRgb('')
  }

  return (
    <>
      <div>
        <b>Hex: </b><input value={newRgb}
        onChange={handleRgbChange} 
        />
      </div>
      <div>
        <button onClick={handleButtonClick}>Convert</button>
      </div>
      <div>
        Hex to RGB: <b>{data}</b>
      </div>
    </>
  )
}
export default HexToRgb