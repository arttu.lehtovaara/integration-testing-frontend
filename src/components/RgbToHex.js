import { useState } from 'react'
import service from '../service/axios'

const RgbToHex = () => {
  const [newRed, setNewRed] = useState()
  const [newGreen, setNewGreen] = useState()
  const [newBlue, setNewBlue] = useState()
  const [data, setData] = useState()

  const handleRedChange = (event) => {
    setNewRed(event.target.value)
  }
  const handleGreenChange = (event) => {
    setNewGreen(event.target.value)
  }
  const handleBlueChange = (event) => {
    setNewBlue(event.target.value)
  }
  const handleButtonClick = () => {
    service
      .getHex(newRed, newGreen, newBlue)
        .then(data => {
          setData(data)
    })
    setNewRed('')
    setNewGreen('')
    setNewBlue('')
  }
  return (
    <>
      <div>
        <b>Rgb</b>
        <br />
        <b>Red: </b><input value={newRed} onChange={handleRedChange}/>
        <br />
        <b>Green: </b><input value={newGreen} onChange={handleGreenChange} />
        <br />
        <b>Blue: </b><input value={newBlue} onChange={handleBlueChange} />
      </div>
      <div>
        <button onClick={handleButtonClick}>Convert</button>
      </div>
      <div>
        <p>RGB to Hex: <b>{data}</b></p>
      </div>
    </>
  )
}
export default RgbToHex